from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from beers.models import Beer

# Create your views here.
@login_required
def beer_list_view(request):

    beer_list = Beer.objects.all()
    beer_list_counter = beer_list.count()
    # beer_list = Beer.objects.filter(pk__in=[])
    # print("beer_list_counter", beer_list_counter)

    # print("exists?", beer_list.filter(id=1))
    # print("exists?", beer_list.filter(id=1).exists())

    # company = Company.objects.create(name="co", tax_number=45678)
    # Beer.objects.create(name="prubea", company=company)

    # company = Company.objects.get(pk=1)
    # print(beer_list.filter(company__name__startswith='H', abv__gte=5))
    # print(beer_list.filter(Q(company__name__startswith='H') | Q(abv__gte=5))) # requiere django.db.models.Q

    #Beer.objects.filter(pk=1).first().delete())

    # print(company.beers) # Estamos llamando al related_name de la ForeignKey company en el modelo Beers

    # for beer in company.beers.all():
    #   print(beer)
    #   beer.abv = 5.4
    #   beer.save()

    # special = SpecialIngredient(name="romero")
    # special.save()
    # print(special)

    # print(Beer.objects.filter(pk__in=[1,2,3,4,5]).first())
    # ingredient = SpecialIngredient.objects.get(pk=1)
    # beer.special_ingredients.add(ingredient)

 	
    context = {
        'beer_list': Beer.objects.all()
    }
		
    return render(request, 'beers_list.html', context)

def beer_detail_view(request, pk):

    # print("user", request.user)
	
    context = {
        'pk': Beer.objects.get(pk=pk)
    }
		
    return render(request, 'beer_detail.html', context)